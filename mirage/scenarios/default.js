export default function(server) {

  let createContactEmails = function(c) {
    server.create('email-address', { contact: c, type: 'primary'});
    server.create('email-address', { contact: c, type: 'alternate'});
    server.create('email-address', { contact: c, type: 'work'});
  }

  let createContactPhones = function(c) {
    server.create('phone-number', { contact: c, type: 'primary'});
    server.create('phone-number', { contact: c, type: 'alternate'});
    server.create('phone-number', { contact: c, type: 'work'});
  }

  server.logging = true;
  let inbox = server.create('folder', { id: 'inbox', name: 'Inbox', userId: 1 });
  let inboxEmails = server.createList('email', 15, { folderId: inbox.id });
  
  let contacts = server.createList('contact', 5);
  contacts.forEach(c => {
    createContactEmails(c);
    createContactPhones(c);
  });

  server.db.folders.update(inbox.id, {
    emailCount: inboxEmails.length,
    unreadCount: server.db.emails.where({ folderId: inbox.id, read: false }).length
  });

  let spam = server.create('folder', { name: 'Spam', userId: 1, emailCount: 32, unreadCount: 13 });
  let spamEmails = server.createList('email', 32, { folderId: spam.id });
}
