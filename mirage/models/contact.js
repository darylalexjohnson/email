import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
	emailAddress: hasMany('email-address'),
	phoneNumber: hasMany('phone-number')
});
