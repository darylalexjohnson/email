import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
	number() { return faker.phone.phoneNumber() ;}
});
