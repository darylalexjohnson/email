export default function() {

  this.get('/emails', function({emails}, request) {
    return emails.where({ folderId: request.queryParams.folder });
  });

  this.get('/contacts');
  this.get('/contacts/:id');
  this.patch('/contacts/:id');
  
  this.get('/email-addresses/:id');
  this.get('/phone-numbers/:id');

  this.get('/emails/:id');
  this.get('/folders');
  this.get('/folders/:id');

  this.post('/emails');}
