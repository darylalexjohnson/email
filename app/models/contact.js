import Ember from 'ember';
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';

export default Model.extend({
  name: attr('string'),
  phoneNumber: hasMany('phone-number'),
  emailAddress: hasMany('email-address'),

  primaryEmail: Ember.computed.filterBy('emailAddress', 'type', 'primary'),
  alternateEmail: Ember.computed.filterBy('emailAddress', 'type', 'alternate'),
  workEmail: Ember.computed.filterBy('emailAddress', 'type', 'work'),

  primaryPhone: Ember.computed.filterBy('phoneNumber', 'type', 'primary'),
  alternatePhone: Ember.computed.filterBy('phoneNumber', 'type', 'alternate'),
});
