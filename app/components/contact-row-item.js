import Ember from 'ember';

const {
	Component,
	computed
} = Ember;

export default Component.extend({
	tagName: '',

	additionalEmails: computed('contact.emailAddress', function() {
		return this.get('contact.emailAddress.length') - 1;
	}),

	additionalPhones: computed('contact.phoneNumber', function() {
		return this.get('contact.phoneNumber.length') - 1;
	})
});
