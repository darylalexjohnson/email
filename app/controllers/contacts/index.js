import Ember from 'ember';

export default Ember.Controller.extend({
	contactSort: ['name'],
	contacts: Ember.computed.sort('model', 'contactSort'),
});
