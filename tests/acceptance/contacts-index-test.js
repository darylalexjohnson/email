import { test } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts index');

test('visiting user contacts route displays all contacts', function(assert) {
  // Set up the test data
  server.createList('contact', 5);
  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact').length, 5, 'All contacts display');
  });
});

test('user contacts list is sorted by name', function(assert) {
  // Set up the test data
  let contacts = [];
  let testContacts = ['John Armstrong', 'Sarita Rodriguez', 'Pia Johnson', 'Jane Doe', 'John Doe'];
  testContacts.forEach(tc => {
    contacts.pushObject(server.create('contact', {name: tc}));
  });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Jane Doe', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(1) .test-contact-name', 'John Armstrong', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(2) .test-contact-name', 'John Doe', 'Contact list is sorted alphabetically');
  });
});

test('displays info for a contact in each row', function(assert) {
  // Set up the test data
  let contacts = [];
  let testContacts = ['John Armstrong', 'Sarita Rodriguez', 'Pia Johnson', 'Jane Doe', 'John Doe'];
  testContacts.forEach(tc => {
    contacts.pushObject(server.create('contact', {name: tc}));
  });
  
  let addAnna = server.create('contact', {name: 'Ann Smith'});
  server.create('email-address', {type: 'primary', contact: addAnna, address: 'smith@example.com'});
  server.create('email-address', {type: 'alternate', contact: addAnna, address: 'smith@alternate.com'});
  server.create('email-address', {type: 'work', contact: addAnna, address: 'smith@work.com'});
  server.create('phone-number', {type: 'primary', contact: addAnna, number: '+1 555-555-5252'});
  server.create('phone-number', {type: 'alternate', contact: addAnna, number: '+1 555-555-9999'});

  contacts.pushObject(addAnna);
  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Ann Smith', 'Contact row displays contact name');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', 'smith@example.com', 'Contact row displays contact primary email');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', '(+2)', 'Displays additional email count');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '+1 555-555-5252', 'Contact row displays primary phone');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '(+1)', 'Displays additional phone count');
  });
});

// Write the tests
test('name links to edit for a contact', function(assert) {
  let contact = server.create('contact', {name: 'Daryl Johnson'});
  server.create('email-address', {type: 'primary', contact: contact, address: 'daryl@dazawebdesign.com'});

  visit('/contacts');
  click('.test-contact-link:eq(0)');

  andThen(function() {
    assert.equal(currentURL(), `/contacts/${contact.id}`, 'Navigated to the contact');
  });
});

test('primary email is a mailto: link', function(assert) {
  let contact = server.create('contact', {name: 'Daryl Johnson'});
  server.create('email-address', {type: 'primary', contact: contact, address: 'daryl@dazawebdesign.com'});

  visit('/contacts');

  andThen(function() {
    let contactEmail = find('.test-contact:eq(0) .test-contact-primary-email a').text().trim();
    assert.equal(find('.test-contact:eq(0) .test-contact-primary-email a').attr('href'), `mailto:${contactEmail}`, 'Primary email is a mailto link.');
  });
});

test('primary phone is a tel: link', function(assert) {
  let contact = server.create('contact', {name: 'Daryl Johnson'});
  server.create('email-address', {type: 'primary', contact: contact, address: 'daryl@dazawebdesign.com'});
  visit('/contacts');

  andThen(function() {
    let contactPhone = find('.test-contact:eq(0) .test-contact-primary-phone a').text().trim();
    assert.equal(find('.test-contact:eq(0) .test-contact-primary-phone a').attr('href'), `tel:${contactPhone}`, 'Primary phone is a tel link.');
  });
});
